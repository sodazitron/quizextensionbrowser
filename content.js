console.log("..... Get rid of the Quiz .....")

var button = document.getElementsByClassName("btn-next-action")
var server = 'http://localhost:5000/new/skill'

for (var i = 0; i < button.length; i++) {
	button[i].addEventListener("click", readData)
}


function toDataURL(src, callback) {
	var xhttp = new XMLHttpRequest();

	xhttp.onload = function () {
		var fileReader = new FileReader();
		fileReader.onloadend = function () {
			callback(fileReader.result);
		}
		fileReader.readAsDataURL(xhttp.response);
	};

	xhttp.responseType = 'blob';
	xhttp.open('GET', src, true);
	xhttp.send();
}


function readData() {
	let category = document.getElementsByClassName("panel-title pull-left")
	let question = document.getElementById("question-description")
	let answers = document.getElementById("possibilities").children

	let questionObject = new Object()
	let wrong = new Array
	let correct = new Array

	
	questionObject.category = category[0].innerText.substring(3)
	questionObject.question = question.innerText

	if (question.getElementsByTagName("img").length > 0) {
		questionObject.hasImage = true
		questionObject.imagePath = question.getElementsByTagName("img")[0].src

		toDataURL(questionObject.imagePath, function(dataURL) {
            questionObject.imageData = dataURL
        });
	} else {
		questionObject.hasImage = false
		questionObject.imageData = false
	}

	for (let i = 0; i < answers.length; i++) {

		if (answers[i].classList.contains("correct-possibility")) {
			correct.push(answers[i].innerText)
			continue
		}

		wrong.push(answers[i].innerText)
	}
	questionObject.wrong = wrong
	questionObject.correct = correct

	sendData(questionObject)
}


function sendData(final) {

	console.log(final)

	var xhr = new XMLHttpRequest()

	xhr.open("POST", server, true)
	xhr.setRequestHeader('Content-Type', 'application/json')
	xhr.send(JSON.stringify(final))

}